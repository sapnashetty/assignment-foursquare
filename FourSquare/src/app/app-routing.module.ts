import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { HomepageComponent } from './components/homepage/homepage.component';
import { MapComponent } from './components/map/map.component';

const routes: Routes = [
  {
    path: 'details',
    loadChildren: './components/details/details.module#DetailsModule'
  },
  {
    path: 'home',
    component: HomepageComponent
  },
  {
    path: '',
    component: MapComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
