import { Component, OnInit } from '@angular/core';

import { FoodService } from '../../services/food.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  searchData: any;
  mapLocation: any;
  latlong: object;

  constructor(private foodService: FoodService, private router: Router) { }
  onEnter(value: string) {
    this.searchData = value;
    this.foodService.updateSearch(this.searchData).subscribe(
      (data: any) => 
      {
        this.mapLocation = data;
        this.latlong = this.mapLocation.results[0].locations[0].latLng;
        this.foodService.updateLatLong(this.latlong);
        this.router.navigate(['home']);
      });

  }

  ngOnInit() { }

}
