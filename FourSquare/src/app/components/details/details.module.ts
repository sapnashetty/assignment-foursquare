import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';

import { DetailsRoutingModule } from './details-routing.module';
import { SwiperComponent } from './swiper/swiper.component';
import { HotelDetailsComponent } from './hotel-details/hotel-details.component';

@NgModule({
  imports: [
    CommonModule,
    DetailsRoutingModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyDXQHwv1ngRO-GGHqxteyP5JHHneUoxxzE'})
  ],
  exports: [SwiperComponent, HotelDetailsComponent],
  declarations: [SwiperComponent, HotelDetailsComponent]
})
export class DetailsModule { }
