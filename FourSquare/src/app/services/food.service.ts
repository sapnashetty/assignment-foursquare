
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Constants } from '../models/constants';
@Injectable({
  providedIn: 'root'
})

export class FoodService {
  latitude;
  longitude;
  search;
  constructor(private http: HttpClient) { }

getFood() {
  return this.http.get(environment.foodUrlStartVenue + Constants.search +
    Constants.oathToken + Constants.foodUrlEnd + this.latitude + ',' + this.longitude + Constants.categoryId);
}

updateSearch(searchData: any) {
  this.search = searchData;
  return this.http.get(environment.locationUrl + this.search);
}

getVenueData(venueId: any) {
  return this.http.get
  (environment.foodUrlStartVenue + venueId + Constants.oathToken + Constants.apiVersion + this.latitude + ',' + this.longitude);
}

getUserReviews(venueId: any) {
  return this.http.get(environment.foodUrlStartVenue + venueId +
    Constants.tips + Constants.oathToken + Constants.apiVersion + this.latitude + ',' + this.longitude + Constants.categoryId);
}

updateLatLong(latlng: any) {
  console.log('latlng', latlng);
  this.latitude = latlng.lat;
  this.longitude = latlng.lng;
}

getRestaurantPhotos(venueId: any) {
  return this.http.get(
    environment.foodUrlStartVenue +
    venueId +
    Constants.photos +
    Constants.oathToken +
    Constants.apiVersion + this.latitude + ',' + this.longitude +
    Constants.categoryId
  );
}
}
