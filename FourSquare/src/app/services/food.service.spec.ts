import { TestBed } from '@angular/core/testing';

import { Food.Service } from './food.service';

describe('Food.Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Food.Service = TestBed.get(Food.Service);
    expect(service).toBeTruthy();
  });
});
